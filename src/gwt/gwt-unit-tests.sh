#!/usr/bin/env bash

cd /root/rstudio/src/gwt

echo "Running ant unittest target..."

ant -Dbuild.dir=bin \
    -Dwww.dir=www \
    -Dextras.dir=extras \
    -Dlib.dir=lib \
    unittest


