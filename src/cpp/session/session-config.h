/*
 * config.h.in
 *
 * Copyright (C) 2009-18 by RStudio, Inc.
 *
 * Unless you have received this program directly from RStudio pursuant
 * to the terms of a commercial license agreement with RStudio, then
 * this program is licensed to you under the terms of version 3 of the
 * GNU Affero General Public License. This program is distributed WITHOUT
 * ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
 * AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
 *
 */

#define RSTUDIO_VERSION "99.9.9"
#define RSTUDIO_GIT_REVISION_HASH "c4098738942318fc17913bce316e88b71c6edeca"
#define RSTUDIO_GIT_COMMIT "c4098738942318fc17913bce316e88b71c6edeca"
#define RSTUDIO_BUILD_ID "unknown"
#define RSTUDIO_BUILD_DATE "2019-09-08"
#define RSTUDIO_COPYRIGHT_YEAR "2019"
#define RSTUDIO_SERVER
#define RSTUDIO_UNVERSIONED_BUILD
/* #undef TRACE_PACKRAT_OUTPUT */

