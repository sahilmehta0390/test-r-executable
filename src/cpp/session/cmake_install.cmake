# Install script for directory: /root/rstudio/src/cpp/session

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local/lib/rstudio-server")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM FILES "/root/rstudio/build/src/cpp/session/r-ldpath")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/rsession" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/rsession")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/rsession"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/root/rstudio/build/src/cpp/session/rsession")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/rsession" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/rsession")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/rsession"
         OLD_RPATH "/usr/lib64/R/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/rsession")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE FILE FILES
    "/root/rstudio/src/cpp/session/resources/markdown.html"
    "/root/rstudio/src/cpp/session/resources/markdown_help.html"
    "/root/rstudio/src/cpp/session/resources/mathjax.html"
    "/root/rstudio/src/cpp/session/resources/pandoc_template.html"
    "/root/rstudio/src/cpp/session/resources/plot_publish.html"
    "/root/rstudio/src/cpp/session/resources/r_highlight.html"
    "/root/rstudio/src/cpp/session/resources/roxygen_help.html"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE FILE FILES
    "/root/rstudio/src/cpp/session/resources/R.css"
    "/root/rstudio/src/cpp/session/resources/markdown.css"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/src/cpp/session/resources/templates")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE FILE FILES "/root/rstudio/src/cpp/session/resources/propagate_scroll.js")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/src/cpp/session/resources/presentation")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE FILE FILES "/root/rstudio/src/cpp/session/resources/NOTICE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE FILE FILES "/root/rstudio/build/src/cpp/session/CITATION")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources/themes" TYPE FILE FILES
    "/root/rstudio/src/cpp/session/resources/themes/ambiance.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/chaos.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/chrome.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/clouds.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/clouds_midnight.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/cobalt.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/compile-themes.R"
    "/root/rstudio/src/cpp/session/resources/themes/crimson_editor.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/dawn.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/dracula.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/dreamweaver.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/eclipse.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/idle_fingers.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/katzenmilch.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/kr_theme.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/material.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/merbivore.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/merbivore_soft.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/mono_industrial.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/monokai.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/pastel_on_dark.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/solarized_dark.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/solarized_light.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/textmate.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/tomorrow.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/tomorrow_night.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/tomorrow_night_blue.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/tomorrow_night_bright.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/tomorrow_night_eighties.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/twilight.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/vibrant_ink.rstheme"
    "/root/rstudio/src/cpp/session/resources/themes/xcode.rstheme"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./R/modules" TYPE FILE FILES
    "/root/rstudio/build/src/cpp/session/modules/R/ModuleTools.R"
    "/root/rstudio/build/src/cpp/session/modules/R/NotebookAlternateEngines.R"
    "/root/rstudio/build/src/cpp/session/modules/R/NotebookData.R"
    "/root/rstudio/build/src/cpp/session/modules/R/NotebookErrors.R"
    "/root/rstudio/build/src/cpp/session/modules/R/NotebookHtmlWidgets.R"
    "/root/rstudio/build/src/cpp/session/modules/R/NotebookPlots.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionAskPass.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionAskSecret.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionAuthoring.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionBreakpoints.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionBuild.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionClang.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionCodeTools.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionCompileAttributes.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionCompletionHooks.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionConnections.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionConnectionsInstaller.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionConsole.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionDataImport.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionDataImportV2.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionDataPreview.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionDataViewer.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionDiagnostics.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionEnvironment.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionErrors.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionFiles.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionHTMLPreview.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionHelp.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionJobs.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionMarkers.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionObjectExplorer.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionOverlay.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionPackages.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionPackrat.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionPatches.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionPlots.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionPlumberViewer.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionPresentation.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionProfiler.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionProjectTemplate.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionRAddins.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionRCompletions.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionRHooks.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionRMarkdown.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionRSConnect.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionRUtil.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionRenv.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionReticulate.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionRmdNotebook.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionShinyViewer.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionSource.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionSpelling.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionSql.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionStan.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionTests.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionThemes.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionUpdates.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SessionUserCommands.R"
    "/root/rstudio/build/src/cpp/session/modules/R/SourceWithProgress.R"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/dependencies/common/dictionaries")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/dependencies/common/mathjax-26")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/src/cpp/session/resources/connections")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/src/cpp/session/resources/schema")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/pandoc" TYPE FILE PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE FILES
    "/root/rstudio/dependencies/common/pandoc/2.3.1/pandoc"
    "/root/rstudio/dependencies/common/pandoc/2.3.1/pandoc-citeproc"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./R/packages" TYPE FILE FILES "/root/rstudio/dependencies/common/renv_0.7.0-39_27deb22dff1abf1d535bcc7a40c66caab5585731.tar.gz")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/src/cpp/session/resources/pdfjs")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/src/cpp/session/resources/grid")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/src/cpp/session/resources/help_resources")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/src/cpp/session/resources/pagedtable")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/./resources" TYPE DIRECTORY FILES "/root/rstudio/src/cpp/session/resources/profiler")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/root/rstudio/build/src/cpp/session/workers/cmake_install.cmake")
  include("/root/rstudio/build/src/cpp/session/postback/cmake_install.cmake")

endif()

