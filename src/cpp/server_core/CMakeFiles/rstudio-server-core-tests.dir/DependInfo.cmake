# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/rstudio/src/cpp/server_core/TestMain.cpp" "/root/rstudio/build/src/cpp/server_core/CMakeFiles/rstudio-server-core-tests.dir/TestMain.cpp.o"
  "/root/rstudio/src/cpp/server_core/UrlPortsTests.cpp" "/root/rstudio/build/src/cpp/server_core/CMakeFiles/rstudio-server-core-tests.dir/UrlPortsTests.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ASIO_DISABLE_KQUEUE"
  "BOOST_ENABLE_ASSERT_HANDLER"
  "BOOST_SIGNALS_NO_DEPRECATION_WARNING"
  "RSTUDIO_BOOST_NAMESPACE=rstudio_boost"
  "RSTUDIO_BOOST_SIGNALS_VERSION=2"
  "RSTUDIO_UNIT_TESTS_ENABLED"
  "WEBSOCKETPP_STRICT_MASKING"
  "_FORTIFY_SOURCE=2"
  "_WEBSOCKETPP_NO_CPP11_MEMORY_=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src/cpp/server_core/include"
  "src/cpp/server_core"
  "../src/cpp/tests/cpp"
  "../src/cpp/core/include"
  "/opt/rstudio-tools/boost/boost_1_69_0/include"
  "/opt/rstudio-tools/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad/third_party/mini_chromium/mini_chromium"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/DependInfo.cmake"
  "/root/rstudio/build/src/cpp/server_core/CMakeFiles/rstudio-server-core.dir/DependInfo.cmake"
  "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
