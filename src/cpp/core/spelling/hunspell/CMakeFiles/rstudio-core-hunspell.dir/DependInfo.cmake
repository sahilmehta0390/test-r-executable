# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/rstudio/src/cpp/core/spelling/hunspell/affentry.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/affentry.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/affixmgr.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/affixmgr.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/csutil.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/csutil.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/dictmgr.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/dictmgr.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/filemgr.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/filemgr.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/hashmgr.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/hashmgr.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/hunspell.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/hunspell.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/hunzip.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/hunzip.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/phonet.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/phonet.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/replist.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/replist.cxx.o"
  "/root/rstudio/src/cpp/core/spelling/hunspell/suggestmgr.cxx" "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/suggestmgr.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ASIO_DISABLE_KQUEUE"
  "BOOST_ENABLE_ASSERT_HANDLER"
  "BOOST_SIGNALS_NO_DEPRECATION_WARNING"
  "HUNSPELL_STATIC"
  "RSTUDIO_BOOST_NAMESPACE=rstudio_boost"
  "RSTUDIO_BOOST_SIGNALS_VERSION=2"
  "RSTUDIO_UNIT_TESTS_ENABLED"
  "WEBSOCKETPP_STRICT_MASKING"
  "_FORTIFY_SOURCE=2"
  "_WEBSOCKETPP_NO_CPP11_MEMORY_=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/cpp/core/spelling/hunspell"
  "/opt/rstudio-tools/boost/boost_1_69_0/include"
  "/opt/rstudio-tools/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad/third_party/mini_chromium/mini_chromium"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
