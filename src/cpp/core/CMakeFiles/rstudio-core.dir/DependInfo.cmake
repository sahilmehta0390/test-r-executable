# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/root/rstudio/src/cpp/core/markdown/sundown/autolink.c" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/sundown/autolink.c.o"
  "/root/rstudio/src/cpp/core/markdown/sundown/buffer.c" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/sundown/buffer.c.o"
  "/root/rstudio/src/cpp/core/markdown/sundown/houdini_href_e.c" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/sundown/houdini_href_e.c.o"
  "/root/rstudio/src/cpp/core/markdown/sundown/houdini_html_e.c" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/sundown/houdini_html_e.c.o"
  "/root/rstudio/src/cpp/core/markdown/sundown/html.c" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/sundown/html.c.o"
  "/root/rstudio/src/cpp/core/markdown/sundown/html_smartypants.c" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/sundown/html_smartypants.c.o"
  "/root/rstudio/src/cpp/core/markdown/sundown/markdown.c" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/sundown/markdown.c.o"
  "/root/rstudio/src/cpp/core/markdown/sundown/stack.c" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/sundown/stack.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BOOST_ASIO_DISABLE_KQUEUE"
  "BOOST_ENABLE_ASSERT_HANDLER"
  "BOOST_SIGNALS_NO_DEPRECATION_WARNING"
  "RSTUDIO_BOOST_NAMESPACE=rstudio_boost"
  "RSTUDIO_BOOST_SIGNALS_VERSION=2"
  "RSTUDIO_UNIT_TESTS_ENABLED"
  "WEBSOCKETPP_STRICT_MASKING"
  "_FORTIFY_SOURCE=2"
  "_WEBSOCKETPP_NO_CPP11_MEMORY_=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../src/cpp/core/include"
  "../src/cpp/ext"
  "src/cpp/core"
  "../src/cpp/tests/cpp"
  "/opt/rstudio-tools/boost/boost_1_69_0/include"
  "/opt/rstudio-tools/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad/third_party/mini_chromium/mini_chromium"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/rstudio/src/cpp/core/Assert.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Assert.cpp.o"
  "/root/rstudio/src/cpp/core/Backtrace.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Backtrace.cpp.o"
  "/root/rstudio/src/cpp/core/Base64.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Base64.cpp.o"
  "/root/rstudio/src/cpp/core/BoostErrors.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/BoostErrors.cpp.o"
  "/root/rstudio/src/cpp/core/BrowserUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/BrowserUtils.cpp.o"
  "/root/rstudio/src/cpp/core/ColorUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/ColorUtils.cpp.o"
  "/root/rstudio/src/cpp/core/ConfigProfile.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/ConfigProfile.cpp.o"
  "/root/rstudio/src/cpp/core/ConfigUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/ConfigUtils.cpp.o"
  "/root/rstudio/src/cpp/core/CrashHandler.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/CrashHandler.cpp.o"
  "/root/rstudio/src/cpp/core/DateTime.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/DateTime.cpp.o"
  "/root/rstudio/src/cpp/core/Error.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Error.cpp.o"
  "/root/rstudio/src/cpp/core/Exec.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Exec.cpp.o"
  "/root/rstudio/src/cpp/core/ExponentialBackoff.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/ExponentialBackoff.cpp.o"
  "/root/rstudio/src/cpp/core/FileInfo.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/FileInfo.cpp.o"
  "/root/rstudio/src/cpp/core/FileLogWriter.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/FileLogWriter.cpp.o"
  "/root/rstudio/src/cpp/core/FilePath.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/FilePath.cpp.o"
  "/root/rstudio/src/cpp/core/FileSerializer.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/FileSerializer.cpp.o"
  "/root/rstudio/src/cpp/core/FileUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/FileUtils.cpp.o"
  "/root/rstudio/src/cpp/core/GitGraph.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/GitGraph.cpp.o"
  "/root/rstudio/src/cpp/core/Hash.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Hash.cpp.o"
  "/root/rstudio/src/cpp/core/HtmlUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/HtmlUtils.cpp.o"
  "/root/rstudio/src/cpp/core/Log.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Log.cpp.o"
  "/root/rstudio/src/cpp/core/LogOptions.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/LogOptions.cpp.o"
  "/root/rstudio/src/cpp/core/LogWriter.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/LogWriter.cpp.o"
  "/root/rstudio/src/cpp/core/PerformanceTimer.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/PerformanceTimer.cpp.o"
  "/root/rstudio/src/cpp/core/PosixStringUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/PosixStringUtils.cpp.o"
  "/root/rstudio/src/cpp/core/ProgramOptions.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/ProgramOptions.cpp.o"
  "/root/rstudio/src/cpp/core/RecursionGuard.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/RecursionGuard.cpp.o"
  "/root/rstudio/src/cpp/core/RegexUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/RegexUtils.cpp.o"
  "/root/rstudio/src/cpp/core/SafeConvert.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/SafeConvert.cpp.o"
  "/root/rstudio/src/cpp/core/Settings.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Settings.cpp.o"
  "/root/rstudio/src/cpp/core/StderrLogWriter.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/StderrLogWriter.cpp.o"
  "/root/rstudio/src/cpp/core/StringUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/StringUtils.cpp.o"
  "/root/rstudio/src/cpp/core/SyslogLogWriter.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/SyslogLogWriter.cpp.o"
  "/root/rstudio/src/cpp/core/Thread.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Thread.cpp.o"
  "/root/rstudio/src/cpp/core/Trace.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/Trace.cpp.o"
  "/root/rstudio/src/cpp/core/WaitUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/WaitUtils.cpp.o"
  "/root/rstudio/src/cpp/core/YamlUtil.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/YamlUtil.cpp.o"
  "/root/rstudio/src/cpp/core/collection/MruList.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/collection/MruList.cpp.o"
  "/root/rstudio/src/cpp/core/file_lock/AdvisoryFileLock.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/file_lock/AdvisoryFileLock.cpp.o"
  "/root/rstudio/src/cpp/core/file_lock/FileLock.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/file_lock/FileLock.cpp.o"
  "/root/rstudio/src/cpp/core/file_lock/LinkBasedFileLock.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/file_lock/LinkBasedFileLock.cpp.o"
  "/root/rstudio/src/cpp/core/gwt/GwtFileHandler.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/gwt/GwtFileHandler.cpp.o"
  "/root/rstudio/src/cpp/core/gwt/GwtLogHandler.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/gwt/GwtLogHandler.cpp.o"
  "/root/rstudio/src/cpp/core/gwt/GwtSymbolMaps.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/gwt/GwtSymbolMaps.cpp.o"
  "/root/rstudio/src/cpp/core/http/CSRFToken.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/CSRFToken.cpp.o"
  "/root/rstudio/src/cpp/core/http/ChunkParser.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/ChunkParser.cpp.o"
  "/root/rstudio/src/cpp/core/http/ChunkProxy.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/ChunkProxy.cpp.o"
  "/root/rstudio/src/cpp/core/http/Cookie.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/Cookie.cpp.o"
  "/root/rstudio/src/cpp/core/http/FormProxy.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/FormProxy.cpp.o"
  "/root/rstudio/src/cpp/core/http/Header.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/Header.cpp.o"
  "/root/rstudio/src/cpp/core/http/Message.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/Message.cpp.o"
  "/root/rstudio/src/cpp/core/http/MultipartRelated.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/MultipartRelated.cpp.o"
  "/root/rstudio/src/cpp/core/http/Request.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/Request.cpp.o"
  "/root/rstudio/src/cpp/core/http/RequestParser.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/RequestParser.cpp.o"
  "/root/rstudio/src/cpp/core/http/Response.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/Response.cpp.o"
  "/root/rstudio/src/cpp/core/http/SocketProxy.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/SocketProxy.cpp.o"
  "/root/rstudio/src/cpp/core/http/URL.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/URL.cpp.o"
  "/root/rstudio/src/cpp/core/http/UriHandler.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/UriHandler.cpp.o"
  "/root/rstudio/src/cpp/core/http/Util.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/http/Util.cpp.o"
  "/root/rstudio/src/cpp/core/json/Json.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/json/Json.cpp.o"
  "/root/rstudio/src/cpp/core/json/JsonRpc.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/json/JsonRpc.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/CodeCompleteResults.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/CodeCompleteResults.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/Cursor.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/Cursor.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/Diagnostic.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/Diagnostic.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/LibClang.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/LibClang.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/SourceIndex.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/SourceIndex.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/SourceLocation.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/SourceLocation.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/SourceRange.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/SourceRange.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/Token.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/Token.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/TranslationUnit.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/TranslationUnit.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/UnsavedFiles.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/UnsavedFiles.cpp.o"
  "/root/rstudio/src/cpp/core/libclang/Utils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/libclang/Utils.cpp.o"
  "/root/rstudio/src/cpp/core/markdown/Markdown.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/Markdown.cpp.o"
  "/root/rstudio/src/cpp/core/markdown/MathJax.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/markdown/MathJax.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RActiveSessions.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/RActiveSessions.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/REnvironmentPosix.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/REnvironmentPosix.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RPackageInfo.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/RPackageInfo.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RProjectFile.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/RProjectFile.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RSessionContext.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/RSessionContext.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RSessionLaunchProfile.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/RSessionLaunchProfile.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RSourceIndex.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/RSourceIndex.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RTokenizer.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/RTokenizer.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RUserData.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/RUserData.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RVersionsPosix.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/r_util/RVersionsPosix.cpp.o"
  "/root/rstudio/src/cpp/core/spelling/HunspellCustomDictionaries.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/spelling/HunspellCustomDictionaries.cpp.o"
  "/root/rstudio/src/cpp/core/spelling/HunspellDictionaryManager.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/spelling/HunspellDictionaryManager.cpp.o"
  "/root/rstudio/src/cpp/core/spelling/HunspellSpellingEngine.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/spelling/HunspellSpellingEngine.cpp.o"
  "/root/rstudio/src/cpp/core/system/ChildProcessSubprocPoll.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/ChildProcessSubprocPoll.cpp.o"
  "/root/rstudio/src/cpp/core/system/Crypto.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/Crypto.cpp.o"
  "/root/rstudio/src/cpp/core/system/Environment.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/Environment.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixChildProcess.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixChildProcess.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixChildProcessTracker.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixChildProcessTracker.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixEnvironment.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixEnvironment.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixFileScanner.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixFileScanner.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixGroup.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixGroup.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixInterrupts.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixInterrupts.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixLibraryLoader.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixLibraryLoader.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixNfs.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixNfs.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixOutputCapture.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixOutputCapture.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixParentProcessMonitor.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixParentProcessMonitor.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixProcess.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixProcess.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixSched.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixSched.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixShellUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixShellUtils.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixSystem.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixSystem.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixUser.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/PosixUser.cpp.o"
  "/root/rstudio/src/cpp/core/system/Process.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/Process.cpp.o"
  "/root/rstudio/src/cpp/core/system/ShellUtils.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/ShellUtils.cpp.o"
  "/root/rstudio/src/cpp/core/system/System.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/System.cpp.o"
  "/root/rstudio/src/cpp/core/system/Xdg.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/Xdg.cpp.o"
  "/root/rstudio/src/cpp/core/system/file_monitor/FileMonitor.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/file_monitor/FileMonitor.cpp.o"
  "/root/rstudio/src/cpp/core/system/file_monitor/LinuxFileMonitor.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/file_monitor/LinuxFileMonitor.cpp.o"
  "/root/rstudio/src/cpp/core/system/recycle_bin/LinuxRecycleBin.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/system/recycle_bin/LinuxRecycleBin.cpp.o"
  "/root/rstudio/src/cpp/core/terminal/PrivateCommand.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/terminal/PrivateCommand.cpp.o"
  "/root/rstudio/src/cpp/core/tex/TexLogParser.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/tex/TexLogParser.cpp.o"
  "/root/rstudio/src/cpp/core/tex/TexMagicComment.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/tex/TexMagicComment.cpp.o"
  "/root/rstudio/src/cpp/core/tex/TexSynctex.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/tex/TexSynctex.cpp.o"
  "/root/rstudio/src/cpp/core/text/AnsiCodeParser.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/text/AnsiCodeParser.cpp.o"
  "/root/rstudio/src/cpp/core/text/DcfParser.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/text/DcfParser.cpp.o"
  "/root/rstudio/src/cpp/core/text/TemplateFilter.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/text/TemplateFilter.cpp.o"
  "/root/rstudio/src/cpp/core/text/TermBufferParser.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/text/TermBufferParser.cpp.o"
  "/root/rstudio/src/cpp/core/zlib/zlib.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/zlib/zlib.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ASIO_DISABLE_KQUEUE"
  "BOOST_ENABLE_ASSERT_HANDLER"
  "BOOST_SIGNALS_NO_DEPRECATION_WARNING"
  "RSTUDIO_BOOST_NAMESPACE=rstudio_boost"
  "RSTUDIO_BOOST_SIGNALS_VERSION=2"
  "RSTUDIO_UNIT_TESTS_ENABLED"
  "WEBSOCKETPP_STRICT_MASKING"
  "_FORTIFY_SOURCE=2"
  "_WEBSOCKETPP_NO_CPP11_MEMORY_=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src/cpp/core/include"
  "../src/cpp/ext"
  "src/cpp/core"
  "../src/cpp/tests/cpp"
  "/opt/rstudio-tools/boost/boost_1_69_0/include"
  "/opt/rstudio-tools/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad/third_party/mini_chromium/mini_chromium"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
