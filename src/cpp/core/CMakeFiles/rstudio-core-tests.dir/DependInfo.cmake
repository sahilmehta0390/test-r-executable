# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/rstudio/src/cpp/core/Base64Tests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/Base64Tests.cpp.o"
  "/root/rstudio/src/cpp/core/ConfigProfileTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/ConfigProfileTests.cpp.o"
  "/root/rstudio/src/cpp/core/FileLockTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/FileLockTests.cpp.o"
  "/root/rstudio/src/cpp/core/FilePathTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/FilePathTests.cpp.o"
  "/root/rstudio/src/cpp/core/MiscellaneousTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/MiscellaneousTests.cpp.o"
  "/root/rstudio/src/cpp/core/StringUtilsTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/StringUtilsTests.cpp.o"
  "/root/rstudio/src/cpp/core/TestMain.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/TestMain.cpp.o"
  "/root/rstudio/src/cpp/core/VersionTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/VersionTests.cpp.o"
  "/root/rstudio/src/cpp/core/Win32FileLockTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/Win32FileLockTests.cpp.o"
  "/root/rstudio/src/cpp/core/http/ChunkParserTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/http/ChunkParserTests.cpp.o"
  "/root/rstudio/src/cpp/core/http/RequestParserTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/http/RequestParserTests.cpp.o"
  "/root/rstudio/src/cpp/core/http/URLTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/http/URLTests.cpp.o"
  "/root/rstudio/src/cpp/core/json/JsonTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/json/JsonTests.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RTokenCursorTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/r_util/RTokenCursorTests.cpp.o"
  "/root/rstudio/src/cpp/core/r_util/RTokenizerTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/r_util/RTokenizerTests.cpp.o"
  "/root/rstudio/src/cpp/core/system/ChildProcessSubprocPollTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/system/ChildProcessSubprocPollTests.cpp.o"
  "/root/rstudio/src/cpp/core/system/CryptoTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/system/CryptoTests.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixShellUtilsTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/system/PosixShellUtilsTests.cpp.o"
  "/root/rstudio/src/cpp/core/system/PosixSystemTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/system/PosixSystemTests.cpp.o"
  "/root/rstudio/src/cpp/core/system/ProcessTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/system/ProcessTests.cpp.o"
  "/root/rstudio/src/cpp/core/system/SystemTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/system/SystemTests.cpp.o"
  "/root/rstudio/src/cpp/core/system/Win32PtyTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/system/Win32PtyTests.cpp.o"
  "/root/rstudio/src/cpp/core/system/Win32SystemTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/system/Win32SystemTests.cpp.o"
  "/root/rstudio/src/cpp/core/terminal/PrivateCommandTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/terminal/PrivateCommandTests.cpp.o"
  "/root/rstudio/src/cpp/core/text/AnsiCodeParserTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/text/AnsiCodeParserTests.cpp.o"
  "/root/rstudio/src/cpp/core/text/DcfParserTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/text/DcfParserTests.cpp.o"
  "/root/rstudio/src/cpp/core/text/TermBufferParserTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/text/TermBufferParserTests.cpp.o"
  "/root/rstudio/src/cpp/core/zlib/zlibTests.cpp" "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core-tests.dir/zlib/zlibTests.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ASIO_DISABLE_KQUEUE"
  "BOOST_ENABLE_ASSERT_HANDLER"
  "BOOST_SIGNALS_NO_DEPRECATION_WARNING"
  "RSTUDIO_BOOST_NAMESPACE=rstudio_boost"
  "RSTUDIO_BOOST_SIGNALS_VERSION=2"
  "RSTUDIO_UNIT_TESTS_ENABLED"
  "WEBSOCKETPP_STRICT_MASKING"
  "_FORTIFY_SOURCE=2"
  "_WEBSOCKETPP_NO_CPP11_MEMORY_=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src/cpp/core/include"
  "../src/cpp/ext"
  "src/cpp/core"
  "../src/cpp/tests/cpp"
  "/opt/rstudio-tools/boost/boost_1_69_0/include"
  "/opt/rstudio-tools/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad/third_party/mini_chromium/mini_chromium"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/DependInfo.cmake"
  "/root/rstudio/build/src/cpp/core/tex/synctex/CMakeFiles/rstudio-core-synctex.dir/DependInfo.cmake"
  "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
