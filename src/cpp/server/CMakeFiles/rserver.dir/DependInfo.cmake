# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/rstudio/build/src/cpp/server/ServerAddins.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerAddins.cpp.o"
  "/root/rstudio/src/cpp/server/ServerBrowser.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerBrowser.cpp.o"
  "/root/rstudio/src/cpp/server/ServerErrorCategory.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerErrorCategory.cpp.o"
  "/root/rstudio/src/cpp/server/ServerEval.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerEval.cpp.o"
  "/root/rstudio/src/cpp/server/ServerInit.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerInit.cpp.o"
  "/root/rstudio/src/cpp/server/ServerMain.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerMain.cpp.o"
  "/root/rstudio/src/cpp/server/ServerMainOverlay.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerMainOverlay.cpp.o"
  "/root/rstudio/src/cpp/server/ServerMeta.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerMeta.cpp.o"
  "/root/rstudio/src/cpp/server/ServerOffline.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerOffline.cpp.o"
  "/root/rstudio/src/cpp/server/ServerOptions.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerOptions.cpp.o"
  "/root/rstudio/src/cpp/server/ServerOptionsOverlay.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerOptionsOverlay.cpp.o"
  "/root/rstudio/src/cpp/server/ServerPAMAuth.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerPAMAuth.cpp.o"
  "/root/rstudio/src/cpp/server/ServerPAMAuthOverlay.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerPAMAuthOverlay.cpp.o"
  "/root/rstudio/src/cpp/server/ServerProcessSupervisor.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerProcessSupervisor.cpp.o"
  "/root/rstudio/src/cpp/server/ServerREnvironment.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerREnvironment.cpp.o"
  "/root/rstudio/src/cpp/server/ServerSessionManager.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerSessionManager.cpp.o"
  "/root/rstudio/src/cpp/server/ServerSessionProxy.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerSessionProxy.cpp.o"
  "/root/rstudio/src/cpp/server/ServerSessionProxyOverlay.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/ServerSessionProxyOverlay.cpp.o"
  "/root/rstudio/src/cpp/server/auth/ServerAuthHandler.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/auth/ServerAuthHandler.cpp.o"
  "/root/rstudio/src/cpp/server/auth/ServerSecureUriHandler.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/auth/ServerSecureUriHandler.cpp.o"
  "/root/rstudio/src/cpp/server/auth/ServerValidateUser.cpp" "/root/rstudio/build/src/cpp/server/CMakeFiles/rserver.dir/auth/ServerValidateUser.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ASIO_DISABLE_KQUEUE"
  "BOOST_ENABLE_ASSERT_HANDLER"
  "BOOST_SIGNALS_NO_DEPRECATION_WARNING"
  "RSTUDIO_BOOST_NAMESPACE=rstudio_boost"
  "RSTUDIO_BOOST_SIGNALS_VERSION=2"
  "RSTUDIO_UNIT_TESTS_ENABLED"
  "WEBSOCKETPP_STRICT_MASKING"
  "_FORTIFY_SOURCE=2"
  "_WEBSOCKETPP_NO_CPP11_MEMORY_=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src/cpp/server/include"
  "src/cpp/server"
  "../src/cpp/core/include"
  "../src/cpp/server_core/include"
  "../src/cpp/monitor/include"
  "../src/cpp/session/include"
  "/opt/rstudio-tools/boost/boost_1_69_0/include"
  "/opt/rstudio-tools/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad"
  "/opt/rstudio-tools/crashpad/crashpad/third_party/mini_chromium/mini_chromium"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/rstudio/build/src/cpp/core/CMakeFiles/rstudio-core.dir/DependInfo.cmake"
  "/root/rstudio/build/src/cpp/server_core/CMakeFiles/rstudio-server-core.dir/DependInfo.cmake"
  "/root/rstudio/build/src/cpp/monitor/CMakeFiles/rstudio-monitor.dir/DependInfo.cmake"
  "/root/rstudio/build/src/cpp/core/spelling/hunspell/CMakeFiles/rstudio-core-hunspell.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
